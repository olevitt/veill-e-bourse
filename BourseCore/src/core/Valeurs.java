package core;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.text.StyledEditorKit.ForegroundAction;


public class Valeurs {

	public ValeursCouleurs prix = new ValeursCouleurs();
	public ValeursCouleurs stock = new ValeursCouleurs();
	public int couleurPrime =  (int) (Math.random() * 5);
	public long tempsPrimeRestant = TEMPS_DEPART;
	public List<Item> enVente = new ArrayList<Item>();
	public static final int TEMPS_DEPART = 60*12;
	transient Timer timer;
	transient  boolean timerRunning = false;
	public int profit = 0;
	public double volatilite = 1;

	public static final int[] reference = new int[]{19,9,4,29,19};

	public Valeurs() {
		timer = new Timer();
		startTimer();
	}
	
	
	

	public int getProfit() {
		return profit;
	}




	public void setProfit(int profit) {
		this.profit = profit;
	}




	public boolean isTimerRunning() {
		return timerRunning;
	}
	
	


	public double getVolatilite() {
		return volatilite;
	}




	public void setVolatilite(double volatilite) {
		this.volatilite = volatilite;
	}




	public void startTimer() {
		if (timerRunning) {
			return;
		}
		timerRunning = true;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (tempsPrimeRestant < 0) {
					tempsPrimeRestant = TEMPS_DEPART;
					couleurPrime = (int) (Math.random() * 5);
				}
				tempsPrimeRestant--;
			}
		}, 1000, 1000);
	}
	
	public void stopTimer() {
		timerRunning = false;
		timer.cancel();
		timer.purge();
	}

	public void recalculerPrix() {
		int stockTotal = 0;
		for (int i : stock.getValeurs()) {
			stockTotal += i;
		}
		for (int i = 0; i < 5; i++) {
			prix.setValeur(i,1 + (int) ((1+(volatilite * ((stock.getValeurs().get(i)+0.0f) / (stockTotal / 5)-1))) * reference[i]));
		}
	}

	public long getTempsPrimeRestant() {
		return tempsPrimeRestant;
	}
	public void setTempsPrimeRestant(long tempsPrimeRestant) {
		this.tempsPrimeRestant = tempsPrimeRestant;
	}
	public int getCouleurPrime() {
		return couleurPrime;
	}
	public void setCouleurPrime(int couleurPrime) {
		this.couleurPrime = couleurPrime;
	}
	public ValeursCouleurs getPrix() {
		return prix;
	}
	public void setPrix(ValeursCouleurs prix) {
		this.prix = prix;
	}
	public ValeursCouleurs getStock() {
		return stock;
	}
	public void setStock(ValeursCouleurs stock) {
		this.stock = stock;
	}
	public List<Item> getEnVente() {
		return enVente;
	}
	public void setEnVente(List<Item> enVente) {
		this.enVente = enVente;
	}



}
