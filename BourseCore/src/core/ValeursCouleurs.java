package core;

import java.util.ArrayList;
import java.util.List;

public class ValeursCouleurs {

	List<Integer> valeurs = new ArrayList<Integer>();

	public ValeursCouleurs() {
		this(1, 2, 3, 4, 5);	
	}
	
	public ValeursCouleurs(int blanc, int bleu, int noir, int rouge, int vert) {
		super();
		valeurs.add(blanc);
		valeurs.add(bleu);
		valeurs.add(noir);
		valeurs.add(rouge);
		valeurs.add(vert);
	}
	
	public void changer(int couleur,int valeur) {
		valeurs.set(couleur, valeurs.get(couleur) + valeur);
	}
	
	public void setValeur(int couleur, int valeur) {
		valeurs.set(couleur, valeur);
	}

	public int getBlanc() {
		return valeurs.get(0);
	}

	public void setBlanc(int blanc) {
		valeurs.set(0, blanc);
	}

	public int getBleu() {
		return valeurs.get(1);
	}

	public void setBleu(int bleu) {
		valeurs.set(1, bleu);
	}

	public int getNoir() {
		return valeurs.get(2);
	}

	public void setNoir(int noir) {
		valeurs.set(2, noir);
	}

	public int getRouge() {
		return valeurs.get(3);
	}

	public void setRouge(int rouge) {
		valeurs.set(3, rouge);
	}

	public int getVert() {
		return valeurs.get(4);
	}

	public void setVert(int vert) {
		valeurs.set(4, vert);
	}

	public List<Integer> getValeurs() {
		return valeurs;
	}

	public void setValeurs(List<Integer> valeurs) {
		this.valeurs = valeurs;
	}
	
	
	
}
