package core;

import java.util.ArrayList;
import java.util.List;

public class Item {

	public int prix;
	public List<Integer> couleurs = new ArrayList<Integer>();
	
	public Item(Valeurs valeursRef) {
		
		int i = 0;
		int nbCartes = 2;
		double rand = Math.random();
		if (rand > 0.3) {
			nbCartes++;
		}
		if (rand > 0.6) {
			nbCartes++;
		}
		if (rand > 0.9) {
			nbCartes++;
		}
		while (i < nbCartes) {
			couleurs.add((int) (Math.random() * 5));
			i++;
		}
		int prixTotal = 0;
		for (int j : couleurs) {
			prixTotal += valeursRef.getPrix().getValeurs().get(j);
		}
		double multiplicateur = 0.8;
		if (nbCartes == 3) {
			multiplicateur = 0.85;
		}
		if (nbCartes == 4) {
			multiplicateur = 0.90;
		}
		if (nbCartes == 5) {
			multiplicateur = 0.95;
		}
		prix = (int) (prixTotal * multiplicateur);
	}
	
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public List<Integer> getCouleurs() {
		return couleurs;
	}
	public void setCouleurs(List<Integer> couleurs) {
		this.couleurs = couleurs;
	}
	
	
}
