package ressources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.GsonBuilder;

import core.Item;
import core.Valeurs;
import core.ValeursCouleurs;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/bourse")
public class HelloWorldResource {

	public static Valeurs valeurs; 
	
	// The Java method will process HTTP GET requests
	@GET 
	@Produces("application/json")
	@Path("test")
	public String getClichedMessage() {
		System.out.println("test");
		return "{owi;à}";
	}
	
	@GET
	@Produces("application/json")
	@Path("achat")
	public String achat(@QueryParam("idAchat") int idAchat) {
		System.out.println("achat : "+idAchat);
		try {
			Item item = valeurs.getEnVente().remove(idAchat);
			valeurs.profit -= item.getPrix();
			ValeursCouleurs stock = valeurs.getStock();
			for (Integer i : item.getCouleurs()) {
				stock.changer(i,-1);
			}
			valeurs.getEnVente().add(new Item(valeurs));
		}
		catch (Exception e) {
			
		}
		return "{}";
	}
	
	@GET
	@Produces("application/json")
	@Path("vente")
	public String vente(@QueryParam("idAchat") int idAchat) {
		System.out.println("vente : "+idAchat);
		try {
			ValeursCouleurs stock = valeurs.getStock();
			valeurs.profit += valeurs.getPrix().getValeurs().get(idAchat);
			stock.changer(idAchat, 1);
		}
		catch (Exception e) {
			
		}
		return "{}";
	}
	
	@GET
	@Produces("application/json")
	@Path("timer")
	public String toggleTimer() {
		System.out.println("timer : "+valeurs.isTimerRunning());
		if (valeurs.isTimerRunning()) {
			valeurs.stopTimer();
		}
		else {
			valeurs.startTimer();
		}
		return "{}";
	}
	
	@GET
	@Produces("application/json")
	@Path("valeurs")
	public String getValeurs() {
		System.out.println("valeurs");
		valeurs.recalculerPrix();
		return new GsonBuilder().create().toJson(valeurs);
	}
}