package webserver;

import java.io.Console;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URI;
import java.util.Enumeration;
import java.util.Scanner;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;

import ressources.HelloWorldResource;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

import core.Item;
import core.Valeurs;
import core.ValeursCouleurs;

public class WebServer {

	static int port = 9999;
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://0.0.0.0/").port(port).build();
	}

	public static final URI BASE_URI = getBaseURI();

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly...");

		ResourceConfig rc = new PackagesResourceConfig("ressources");
		return GrizzlyServerFactory.createHttpServer(BASE_URI,rc);
	}

	public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Nombre de joueurs");
		int nbJoueurs = scanner.nextInt();
		System.out.println("Nombre de cartes par joueurs");
		int nbCarteParJoueur = scanner.nextInt();

		int nbCarteTotal = nbCarteParJoueur * nbJoueurs;
		Valeurs valeurs = new Valeurs();
		valeurs.setStock(new ValeursCouleurs(nbCarteTotal,nbCarteTotal,nbCarteTotal,nbCarteTotal,nbCarteTotal));
		
		HelloWorldResource.valeurs = valeurs;
		valeurs.recalculerPrix();
		for (int i = 0; i < 4; i++) {
			valeurs.getEnVente().add(new Item(valeurs));
		}
		
		Config.reload(valeurs);
		System.out.println("Enjoy !");
		
		HttpServer httpServer = startServer();
		Enumeration<NetworkInterface> enumera = NetworkInterface.getNetworkInterfaces();
		while (enumera.hasMoreElements()) {
			Enumeration<InetAddress> adresses = enumera.nextElement().getInetAddresses();
			while (adresses.hasMoreElements()) {
				InetAddress adresse = adresses.nextElement();
				if (adresse.isSiteLocalAddress()) {
					System.out.println(adresse.getHostAddress()+":"+port);
				}
			}
		}


		boolean stop = false;
		while (!stop) {
			String ligne = scanner.nextLine();
			if (ligne != null) {
				Config.reload(valeurs);
			}
		}
		System.in.read();
		httpServer.stop();
	}    
}
