package webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import core.Valeurs;

public class Config {

	public static void reload(Valeurs valeurs)  {
		try {
			File file = new File("conf.txt");
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String ligne = null;
			while ((ligne = reader.readLine()) != null) {
				try {
					String[] val = ligne.split("=");
					if (val[0].equals("volatilite")) {
						valeurs.setVolatilite(Double.parseDouble(val[1]));
						System.out.println("Volatilité réglée à "+val[1]);
					}
				}
				catch (Exception e) {
					System.out.println("Ligne "+ligne+" invalide");
				}
			}
		}
		catch (Exception e) {
			System.out.println("Erreur lors de la lecture de la config");
		}
	}
}
