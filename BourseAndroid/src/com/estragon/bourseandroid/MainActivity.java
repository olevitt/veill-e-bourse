package com.estragon.bourseandroid;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import core.Valeurs;

public class MainActivity extends Activity {

	public static AsyncHttpClient client = new AsyncHttpClient();
	Timer timer;
	public static String ip = "http://192.168.0.1:9999";
	public static Valeurs valeurs = new Valeurs();
	PrixAdapter adapter;
	VentesAdapter adapterVentes;
	ListView liste;
	ListView ventes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		ip = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("ip", ip);
		startTimer();
		liste = (ListView) findViewById(R.id.prix);
		liste.setAdapter(adapter = new PrixAdapter(this));
		ventes = (ListView) findViewById(R.id.ventes);
		ventes.setAdapter(adapterVentes = new VentesAdapter(this));
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		stopTimer();
		super.onDestroy();
	}

	protected void onResume() {
		startTimer();
		super.onResume();
	}

	private void stopTimer() {
		try {
			timer.cancel();
			timer.purge();
		}
		catch (Exception e) {
			
		}
	}

	private void startTimer() {
		stopTimer();
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				client.get(ip+"/bourse/valeurs", new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(String response) {
						System.out.println(response);
						valeurs = new Gson().fromJson(response, Valeurs.class);
						runOnUiThread(new Runnable() {
							public void run() {
								adapter.notifyDataSetChanged();
								adapterVentes.notifyDataSetChanged();
							}
						});
						
						
					}
				});
			}
		}, 5000, 2000);
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);

			alert.setTitle("IP du serveur");

			// Set an EditText view to get user input 
			final EditText input = new EditText(this);
			input.setText(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("ip", "192.168.0.1:9999"));
			alert.setView(input);

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					String value = input.getText().toString();
					if (!input.getText().toString().startsWith("http://")) {
						value = "http://"+input.getText().toString();
					}
					
					PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().putString("ip", value).commit();
					ip = value;
				}
			});

			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
				}
			});

			alert.show();
			return super.onOptionsItemSelected(item);
		}
		else if (item.getItemId() == R.id.action_pause) {
			client.get(ip+"/bourse/timer", new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(String response) {
					System.out.println(response);
				}
			});
		}
		return true;
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v) {
		client.get(ip+"/bourse/achat?idAchat=1", new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				System.out.println(response);
			}
		});
	}


}
