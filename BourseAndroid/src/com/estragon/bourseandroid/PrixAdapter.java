package com.estragon.bourseandroid;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

public class PrixAdapter extends BaseAdapter {

	Context context;
	
	public PrixAdapter(Context context) {
		this.context = context;
	}
	
	public int[] images = new int[] {R.drawable.blanc,R.drawable.bleu,R.drawable.noir,R.drawable.rouge,R.drawable.vert};
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		
		return "";
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View itemView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) parent.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemView = inflater.inflate(R.layout.newitem, null);
		} else {
			itemView = convertView;
		}
		
		ImageView image = (ImageView) itemView.findViewById(R.id.imagecouleur);
		image.setImageResource(images[position]);
		TextView texte = (TextView) itemView.findViewById(R.id.textetitle);
		texte.setTypeface(null, Typeface.BOLD);
		texte.setTextSize(40);
		Button vente = (Button) itemView.findViewById(R.id.vente);
		vente.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainActivity.client.get(MainActivity.ip+"/bourse/vente?idAchat="+position, new AsyncHttpResponseHandler() {

					@Override
					public void onSuccess(String arg0) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0);
						Toast.makeText(context, "Vente ok !", Toast.LENGTH_SHORT).show();
					}
					
				});
			}
		});
		
		System.out.println("Ok"+position);
		if (MainActivity.valeurs != null) {
			texte.setText(MainActivity.valeurs.getPrix().getValeurs().get(position)+"€");
		}
		return itemView;
	}

}
