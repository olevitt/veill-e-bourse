package com.estragon.bourseandroid;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import core.Item;

public class VentesAdapter extends BaseAdapter {

	Context context;
	public int[] images = new int[] {R.drawable.blanc,R.drawable.bleu,R.drawable.noir,R.drawable.rouge,R.drawable.vert};
	public int[] imagesViewsRefs = new int[] {R.id.image1,R.id.image2,R.id.image3,R.id.image4,R.id.image5};
	
	public VentesAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return MainActivity.valeurs.getEnVente().size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return MainActivity.valeurs.getEnVente().get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View itemView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) parent.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemView = inflater.inflate(R.layout.venteitem, null);
		} else {
			itemView = convertView;
		}
		
		Item item = MainActivity.valeurs.getEnVente().get(position);
		for (int i = 0; i < 5; i++) {
			ImageView img = (ImageView) itemView.findViewById(imagesViewsRefs[i]);
			if (i < item.getCouleurs().size()) {
				img.setVisibility(View.VISIBLE);
				img.setImageResource(images[item.getCouleurs().get(i)]);
			}
			else {
				img.setVisibility(View.INVISIBLE);
			}
		}
		TextView texte = (TextView) itemView.findViewById(R.id.textetitle);
		texte.setTypeface(null, Typeface.BOLD);
		texte.setTextSize(40);
		Button vente = (Button) itemView.findViewById(R.id.vente);
		vente.setText("Achat");
		vente.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainActivity.client.get(MainActivity.ip+"/bourse/achat?idAchat="+position, new AsyncHttpResponseHandler() {

					@Override
					public void onSuccess(String arg0) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0);
						Toast.makeText(context, "Achat ok !", Toast.LENGTH_SHORT).show();
					}
					
				});
			}
		});
		
		System.out.println("Ok"+position);
		if (MainActivity.valeurs != null) {
			texte.setText(MainActivity.valeurs.getEnVente().get(position).getPrix()+"€");
		}
		return itemView;
	}

}
