package ui;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;

public class CompteARebours extends JLabel {

	long timeLeft;
	
	public CompteARebours(long init) {
		timeLeft = init;
		setText("Chargement ...");
		setFont(MenuLabel.fontMenu);
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				timeLeft -= 1;
				int minutes = (int) (timeLeft / 60);
				int secondes = (int) (timeLeft % 60);
				setText(String.format("%02d", minutes)+":"+String.format("%02d", secondes));
			}
		}, 1000, 1000);
	}

	public long getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(long timeLeft) {
		this.timeLeft = timeLeft;
	}
	
	
}
