package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import core.Item;
import core.Valeurs;

public class Achat extends JPanel {

	List<UIItem> items = new ArrayList<UIItem>();
	
	public Achat(Valeurs valeurs) {
		GridBagLayout layoutachat = new GridBagLayout();
		setLayout(layoutachat);
		GridBagConstraints constraints = new GridBagConstraints();
		add(new MenuLabel("Achat"),constraints);
		constraints.gridy++;
		for (int i = 0; i < 5; i++) {
			constraints.gridy++;
			UIItem item2 = new UIItem(i < valeurs.getEnVente().size() ? valeurs.getEnVente().get(i) : null);
			items.add(item2);
			add(item2,constraints);
		}
	}
	
	public void setValeurs(Valeurs valeurs) {
		for (int i = 0; i < 5; i++) {
			if (i < valeurs.getEnVente().size()) {
				items.get(i).setVisible(true);
				items.get(i).setItem(valeurs.getEnVente().get(i));
			}
			else {
				items.get(i).setVisible(false);
			}
		}
	}
}
