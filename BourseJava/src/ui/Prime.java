package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JPanel;

import core.Valeurs;

public class Prime extends JPanel {

	Valeurs valeurs;
	private CompteARebours rebours;
	private ImageCouleur couleur;
	private PrixLabel profit = new PrixLabel(0,"€");

	public void setValeurs(Valeurs valeurs) {
		this.valeurs = valeurs;
		rebours.setTimeLeft(valeurs.getTempsPrimeRestant());
		couleur.setType(valeurs.getCouleurPrime());
		profit.setPrix(valeurs.getProfit());
	}

	public Prime(Valeurs valeurs) {
		this.valeurs = valeurs;
		GridBagConstraints constraints = new GridBagConstraints();
		GridBagLayout layoutPrime = new GridBagLayout();
		constraints.gridy++;
		setLayout(layoutPrime);
		add(new MenuLabel("Prime"),constraints);
		constraints.gridy++;
		add(couleur = new ImageCouleur(valeurs.getCouleurPrime()),constraints);
		add(rebours = new CompteARebours(valeurs.getTempsPrimeRestant()),constraints);
		constraints.gridy++;
		add(new MenuLabel("Profits banque"),constraints);
		constraints.gridy++;
		profit.setPrix(valeurs.getProfit());
		add(profit,constraints);
	}
}

