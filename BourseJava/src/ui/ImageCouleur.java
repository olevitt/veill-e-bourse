package ui;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ImageCouleur extends JLabel {

	public static final int BLANC = 0, BLEU = 1, NOIR = 2, ROUGE = 3, VERT = 4;
	int type;
	
	public ImageCouleur(int type) {
		this.type = type;
		setIcon(new ImageIcon(getIconURI()));
	}
	
	public void setType(int type) {
		if (type == this.type) {
			return;
		}
		this.type = type;
		setIcon(new ImageIcon(getIconURI()));
	}
	
	public static URL getIconURI(int type) {
		switch (type) {
		case BLANC:
			return ImageCouleur.class.getResource("/res/blanc.png");
		case BLEU:
			return ImageCouleur.class.getResource("/res/bleu.png");
		case NOIR:
			return ImageCouleur.class.getResource("/res/noir.png");
		case ROUGE:
			return ImageCouleur.class.getResource("/res/rouge.png");
		case VERT:
			return ImageCouleur.class.getResource("/res/vert.png");

		default:
			return ImageCouleur.class.getResource("/res/blanc.png");
		}
	}
	
	public URL getIconURI() {
		return getIconURI(type);
	}
}
