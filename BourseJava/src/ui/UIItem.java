package ui;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import core.Item;

public class UIItem extends JPanel {

	Item item;
	List<JLabel> couleurs = new ArrayList<JLabel>();
	PrixLabel prix;
	
	public UIItem(Item item) {
		for (int i = 0; i < 5; i++) {
			couleurs.add(new JLabel(new ImageIcon(ImageCouleur.getIconURI(1))));
			add(couleurs.get(i));
		}
		add(prix = new PrixLabel(0));
		setItem(item);
		
	}
	
	public void setItem(Item item) {
		if (item == null ){
			return;
		}
		if (item.equals(this.item)) {
			System.out.println("Pas besoin de mettre à jour");
		}
		this.item = item;
		setLayout(new FlowLayout());
		for (int j = 0; j < couleurs.size(); j++) {
			if (j < item.getCouleurs().size()) {
				couleurs.get(j).setVisible(true);
				couleurs.get(j).setIcon(new ImageIcon(ImageCouleur.getIconURI(item.getCouleurs().get(j))));
			}
			else {
				couleurs.get(j).setVisible(false);
			}
		}
		prix.setPrix(item.getPrix());
		
	}
	
}
