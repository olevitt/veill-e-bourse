package ui;

import java.awt.Font;

import javax.swing.JLabel;

public class MenuLabel extends JLabel {

	public static Font fontMenu = new Font("Arial", Font.BOLD, 25);
	
	public MenuLabel(String text) {
		setFont(fontMenu);
		setText(text);
	}
}
