package ui;

import java.awt.Font;

import javax.swing.JLabel;

public class PrixLabel extends JLabel {

	static Font prixFont = new Font("Arial",Font.BOLD,30);
	String suffixe = "€";
	
	public PrixLabel(int prix) {
		this(prix,"€");
	}
	
	public PrixLabel(int prix, String suffixe) {
		setFont(prixFont);
		setSuffixe(suffixe);
		setPrix(prix);
	}
	
	public void setPrix(int prix) {
		setText(prix+suffixe);
	}
	
	public void setSuffixe(String suffixe) {
		this.suffixe = suffixe;
	}

}
