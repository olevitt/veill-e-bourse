package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import core.ValeursCouleurs;

public class AffichageCouleurs extends JPanel {

	ValeursCouleurs valeurs;
	String nom;
	int iteration = 1;
	PrixLabel blanc, bleu, noir, rouge, vert;
	
	public AffichageCouleurs(String nom,ValeursCouleurs valeurs,String suffixe) {
		this.nom = nom;
		this.valeurs = valeurs;
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		setLayout(layout);
		constraints.gridy++;
		add(new MenuLabel(nom),constraints);
		constraints.gridy++;
		add(new ImageCouleur(ImageCouleur.BLANC),constraints);
		add(blanc = new PrixLabel(valeurs.getBlanc(),suffixe),constraints);
		constraints.gridy++;
		add(new ImageCouleur(ImageCouleur.BLEU),constraints);
		add(bleu = new PrixLabel(valeurs.getBleu(),suffixe),constraints);
		constraints.gridy++;
		add(new ImageCouleur(ImageCouleur.NOIR),constraints);
		add(noir = new PrixLabel(valeurs.getNoir(),suffixe),constraints);
		constraints.gridy++;
		add(new ImageCouleur(ImageCouleur.ROUGE),constraints);
		add(rouge = new PrixLabel(valeurs.getRouge(),suffixe),constraints);
		constraints.gridy++;
		add(new ImageCouleur(ImageCouleur.VERT),constraints);
		add(vert = new PrixLabel(valeurs.getVert(),suffixe),constraints);
	}
	
	public AffichageCouleurs(String nom,ValeursCouleurs valeurs) {
		this(nom,valeurs,"€");
	}
	
	private void rafraichir() {
		blanc.setPrix(valeurs.getBlanc());
		bleu.setPrix(valeurs.getBleu());
		noir.setPrix(valeurs.getNoir());
		rouge.setPrix(valeurs.getRouge());
		vert.setPrix(valeurs.getVert());
	}

	public ValeursCouleurs getValeurs() {
		return valeurs;
	}

	public void setValeurs(ValeursCouleurs valeurs) {
		this.valeurs = valeurs;
		rafraichir();
	}
	
	
	
	
}
