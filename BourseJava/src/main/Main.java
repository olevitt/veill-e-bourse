package main;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import com.google.gson.Gson;

import core.Valeurs;

public class Main {

	static GameFrame frame;
	public static Valeurs valeurs = new Valeurs();

	public static void main(String[] args) {
		frame = new GameFrame();
		frame.setTitle("Bourse");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.loadValeurs(valeurs);
		new Timer().scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					URL yahoo = new URL("http://localhost:9999/bourse/valeurs");
					HttpURLConnection yc = (HttpURLConnection) yahoo.openConnection();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(
									yc.getInputStream()));
					String inputLine;
					String total = "";

					while ((inputLine = in.readLine()) != null) 
						total += inputLine;
					
					final Valeurs valeurs = new Gson().fromJson(total, Valeurs.class);
					EventQueue.invokeAndWait(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							frame.loadValeurs(valeurs);
						}
					});
					in.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}, 5000,1000);
		frame.setVisible(true);
	}
}
