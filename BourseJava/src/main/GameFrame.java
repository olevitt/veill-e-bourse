package main;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ui.Achat;
import ui.AffichageCouleurs;
import ui.Prime;
import core.Valeurs;
import core.ValeursCouleurs;

public class GameFrame extends JFrame {

	AffichageCouleurs vente, circulation;
	Prime prime;
	Achat achat;
	
	public GameFrame() {
		vente = new AffichageCouleurs("Vente",new ValeursCouleurs(1, 2, 3, 4, 5));
		prime = new Prime(new Valeurs());
		circulation = new AffichageCouleurs("En circulation",new ValeursCouleurs(5, 4, 3, 2, 1),"");
		achat = new Achat(new Valeurs());
		try {
			EventQueue.invokeAndWait(new Runnable() {
				public void run() {
					GridLayout layoutmain = new GridLayout(2,2);
					
					getContentPane().setLayout(layoutmain);
					getContentPane().add(vente);
					getContentPane().add(achat);
					getContentPane().add(prime);
					getContentPane().add(circulation);
					GridBagConstraints constraints = new GridBagConstraints();
					constraints.weightx = 1;
					constraints.gridy++;
					
					
				}
			});
		}
		catch (Exception e) {
			
		}
	}
	
	public void loadValeurs(Valeurs valeurs) {
		vente.setValeurs(valeurs.getPrix());
		circulation.setValeurs(valeurs.getStock());
		prime.setValeurs(valeurs);
		achat.setValeurs(valeurs);
	}
}
